#include "../../src/datamod.h"

#include <iostream>

int main(int argc, char** argv)
{
    AlkaTest::FindMedian c;

	int s[] = { 1, 2, 3, 4 };
	c.InsertSequence(s, 4);
    std::cout << "Median is: " << c.GetMedian() << std::endl;
 
    return 0;
}