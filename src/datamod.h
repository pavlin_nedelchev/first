#pragma once

#include <vector>

#ifdef _WIN32
#ifdef DATAMOD_EXPORT
#define DllExport   __declspec( dllexport )
#else
#define DllExport   __declspec( dllimport )
#endif
#else
#define DllExport
#endif

namespace AlkaTest
{
    DllExport class FindMedian
    {
    public:
        FindMedian();
        ~FindMedian();

        void InsertNumber(const int& number);
        void InsertNumber(const double& number);

        void InsertSequence(const std::vector<int>& seq);
        void InsertSequence(const std::vector<double>& seq);
        void InsertSequence(const int* seq, const std::size_t size);
        void InsertSequence(const double* seq, const std::size_t size);

        double GetMedian();
        void Clear();

    private:
        std::vector<double> sequence;
    };
}