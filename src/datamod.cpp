#include "datamod.h"
#include <algorithm>

namespace AlkaTest
{
    FindMedian::FindMedian() {};

    FindMedian::~FindMedian()
    {
        sequence.clear();
    }

    void FindMedian::InsertNumber(const int& number)
    {
        sequence.push_back(number);
    }

    void FindMedian::InsertNumber(const double& number)
    {
        sequence.push_back(number);
    }

    void FindMedian::InsertSequence(const std::vector<int>& seq)
    {
        for(auto& i: seq)
            InsertNumber(i);
    }

    void FindMedian::InsertSequence(const std::vector<double>& seq)
    {
        for(auto& i: seq)
            InsertNumber(i);
    }

    void FindMedian::InsertSequence(const int* seq, const std::size_t size)
    {
        for(std::size_t i=0; i < size; i++)
            InsertNumber(seq[i]);
    }

    void FindMedian::InsertSequence(const double* seq, const std::size_t size)
    {
        for(std::size_t i=0; i < size; i++)
            InsertNumber(seq[i]);
    }

    double FindMedian::GetMedian()
    {
		int mid_element = sequence.size() / 2;
		sort(sequence.begin(), sequence.end());
		// odd number in sequence
		if (sequence.size() % 2)
			return sequence[mid_element];

		// even number in sequence: throws an exception if sequence is empty
		return (sequence[mid_element] + sequence[mid_element - 1]) / 2;
    }

    void FindMedian::Clear()
    {
        sequence.clear();
    }
}